import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;



public class View implements ActionListener{
	public JFrame window=new JFrame("Polinoame"); //componente grafice utilizate
	public JLabel coef=new JLabel("Coeficient"); 
	public JTextField coeficient=new JTextField();
	public JLabel put=new JLabel("Putere");
	public JTextField putere=new JTextField();
	public JButton add=new JButton("Add");
	public JButton afis=new JButton("Afisare Polinom");
	public JTextField afisare=new JTextField();
	public JButton adaugaPolinom=new JButton("Adaugare Polinom");
	public JButton sterge=new JButton("Stergere");
	public JButton derivare=new JButton("Derivare");
	public JButton integrare=new JButton("Integrare");
	public JButton aduna=new JButton("Adunare");
	public JButton scade=new JButton("Scadere");
	public JButton inmulteste=new JButton("Inmultire");
	public JButton imparte=new JButton("Impartire");
	public JButton reinitializare=new JButton("Reinitializare");
	public JLabel pct=new JLabel("Punct");
	public JTextField punct=new JTextField();
	public JButton calculare=new JButton("Calculare val. polinom");
	
	int nrPolinom=0; //variabila care contorizeaza numarul de ordine al polinomului
	
	
	ArrayList <Polinom> polinoame=new ArrayList<>();
	ArrayList <Polinom> polinoame2=new ArrayList<>();
	
	Polinom p=new Polinom(); //polinoame folosite in calcul
	Polinom p2=new Polinom();
	Polinom p3=new Polinom();
	public int gradMax; // va determina gradul polinomului
	
	public View(){  //initializare si introducere componente grafice
		            //precum si Listener pentru butoane
		window.setSize(500,230);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setVisible(true);
		window.setLayout(null);
		
		coef.setBounds(10,10,65,20);//setare coordonate
		window.add(coef);           //adaugare in Frame
		
		coeficient.setBounds(80, 10, 40, 20);
		window.add(coeficient);
		
		put.setBounds(130,10,65,20);
		window.add(put);
		
		putere.setBounds(180,10,40,20);
		window.add(putere);
		
		add.setBounds(240, 10, 65, 20);
		window.add(add);
		
		afis.setBounds(10,40,150,20);
		window.add(afis);
		
		afisare.setBounds(170,40,200,20);
		window.add(afisare);
		
		adaugaPolinom.setBounds(310,10,150,20);
		window.add(adaugaPolinom);
		
		sterge.setBounds(380, 40, 100, 20);
		window.add(sterge);
		
		derivare.setBounds(10, 70, 90, 20);
		window.add(derivare);
		
	    integrare.setBounds(110, 70, 90, 20);
	    window.add(integrare);
	    
	    aduna.setBounds(210,70,90,20);
	    window.add(aduna);
	    
	    scade.setBounds(310, 70, 90, 20);
		window.add(scade);
		
		inmulteste.setBounds(60,100,90,20);
		window.add(inmulteste);
		
		imparte.setBounds(250,100,90,20);
		window.add(imparte);
		
		reinitializare.setBounds(135,130,120,20);
		window.add(reinitializare);
		
		pct.setBounds(10, 160, 50, 20);
		window.add(pct);
		
		punct.setBounds(60, 160, 50, 20);
		window.add(punct);
		
		calculare.setBounds(120, 160, 180, 20);
		window.add(calculare);
		
		calculare.addActionListener(this);
		reinitializare.addActionListener(this);   //listener butoane
		imparte.addActionListener(this);
		inmulteste.addActionListener(this);
	    scade.addActionListener(this);
	    aduna.addActionListener(this);
		integrare.addActionListener(this);
		derivare.addActionListener(this);
		sterge.addActionListener(this);
		afis.addActionListener(this);
		adaugaPolinom.addActionListener(this);
		add.addActionListener(this);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		
		
		
		if(e.getSource()==add){  //citire date si apelare construire primul sau al doilea polinom
			if(Integer.parseInt(putere.getText())>gradMax)
				gradMax=Integer.parseInt(putere.getText());   //determinare grad maxim pe
			                                                  //pe parcursul citirii
			
			int p1=Integer.parseInt(putere.getText());        //putere citita
			int coef1=Integer.parseInt(coeficient.getText()); //coeficient citit
		
			gradMax=p1;
			if(nrPolinom==0)
			    p.construire(coef1, p1, gradMax);
			  else 
				p2.construire(coef1, p1, gradMax);
			
			coeficient.setText("");
			putere.setText("");
			
		}
		else if(e.getSource()==adaugaPolinom){  //introducere primul sau al doilea polinom
			if(nrPolinom==0){
				polinoame.add(p);
				nrPolinom=1;
			}
			else
			{
				polinoame.add(p2);
			    nrPolinom=0;
			}
			
		}
		else if(e.getSource()==afis){  //afisare polinom introdus
			String a=p.getString(p);
			afisare.setText(a);
			p.afisPolinom();
			
		}
		else if(e.getSource()==sterge){ //stergere casuta rezultat
			afisare.setText("");
		}
		else if(e.getSource()==derivare){ //operatia de derivare
			Polinom deriv=new Polinom();
			deriv=deriv.derivare(p);
			String a=deriv.getString(deriv);
			afisare.setText(a);
		}
		else if(e.getSource()==integrare){ //operatia de integrare
			Polinom integr=new Polinom();
			integr=integr.integrare(p);
			String a=integr.getString(integr);
			afisare.setText(a);
		}
		else if(e.getSource()==aduna){  //operatia de adunare
            Polinom rez=new Polinom();
            rez=rez.adunare(p, p2);
            String a=rez.getString(rez);
            afisare.setText(a);
		}
		else if(e.getSource()==scade){ //operatia de scadere
			 Polinom rez=new Polinom();
	         rez=rez.scadere(p, p2);
	         String a=rez.getString(rez);
	         afisare.setText(a);
		}
		else if(e.getSource()==inmulteste){  //operatia de inmltire
			 Polinom rez=new Polinom();
	         rez=rez.inmultire(p, p2);
	         String a=rez.getString(rez);
	         afisare.setText(a);
		}
		else if(e.getSource()==imparte){
			Polinom rez=new Polinom();
			rez=rez.impartire(p, p2);
		    String a=rez.getString(rez);
	        afisare.setText(a);
		}
		else if(e.getSource()==calculare){
			int x=p.valoarePolinom(Integer.parseInt(punct.getText()), p);
			punct.setText(Integer.toString(x));
		}
		else if(e.getSource()==reinitializare){ //reinitializeaza datele
			View nou=new View();
		}
		
	}
	
}
