import static org.junit.Assert.*;

import org.junit.Test;

public class Testare {
	
	public Testare(){}
	@Test
	public void testAdunare() {
		
		int coeficienti[]={1,1,2};
		int coeficienti2[]={2,3};
		
		Polinom p=new Polinom(2,coeficienti);
		Polinom p2=new Polinom(1,coeficienti2);
		
		Polinom rez=new Polinom();
		rez=rez.adunare(p, p2);
		
		int coefRezultat[]={3,4,2};
		Polinom rezultat=new Polinom(2,coefRezultat);
		
	    int i=rez.compararePolinoame(rez, rezultat);
		assertEquals(1,i);
	}
	
	@Test
	public void testScadere() {
		
		int coeficienti[]={1,1,2};
		int coeficienti2[]={2,3};
		
		Polinom p=new Polinom(2,coeficienti);
		Polinom p2=new Polinom(1,coeficienti2);
		
		Polinom rez=new Polinom();
		rez=rez.scadere(p, p2);
		
		int coefRezultat[]={-1,-2,2};
		Polinom rezultat=new Polinom(2,coefRezultat);
		
	    int i=rez.compararePolinoame(rez, rezultat);
		assertEquals(1,i);
	}
	
	@Test
	public void testInmultire() {
		
		int coeficienti[]={2,0,1};
		int coeficienti2[]={0,3};
		
		Polinom p=new Polinom(2,coeficienti);
		Polinom p2=new Polinom(1,coeficienti2);
		
		Polinom rez=new Polinom();
		rez=rez.inmultire(p, p2);
		
		int coefRezultat[]={0,6,0,3};
		Polinom rezultat=new Polinom(3,coefRezultat);
		
	    int i=rez.compararePolinoame(rez, rezultat);
		assertEquals(1,i);
	}
	
	@Test
	public void testImpartire() {
		
		int coeficienti[]={2,0,4};
		int coeficienti2[]={0,2};
		
		Polinom p=new Polinom(2,coeficienti);
		Polinom p2=new Polinom(1,coeficienti2);
		
		Polinom rez=new Polinom();
		rez=rez.impartire(p, p2);
		
		int coefRezultat[]={0,2};
		Polinom rezultat=new Polinom(1,coefRezultat);
		
	    int i=rez.compararePolinoame(rez, rezultat);
		assertEquals(1,i);
	}
	
	@Test
	public void testDerivare() {
		
		int coeficienti[]={1,1,2};
		
		Polinom p=new Polinom(2,coeficienti);
		
		Polinom rez=new Polinom();
		rez=rez.derivare(p);
		
		int coefRezultat[]={1,4};
		Polinom rezultat=new Polinom(1,coefRezultat);
		
	    int i=rez.compararePolinoame(rez, rezultat);
		assertEquals(1,i);
	}
	
	@Test
	public void testIntegrare() {
		
		int coeficienti[]={1,1,2};
		
		Polinom p=new Polinom(2,coeficienti);
		
		Polinom rez=new Polinom();
		rez=rez.integrare(p);
		
		int coefRezultat[]={0,1};
		Polinom rezultat=new Polinom(1,coefRezultat);
		
	    int i=rez.compararePolinoame(rez, rezultat);
		assertEquals(1,i);
	}
	

}