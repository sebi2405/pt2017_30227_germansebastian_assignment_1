
public class Polinom {
	public int grad;
	public int []coeficienti={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	//consideram gradul maxim 30 (am adaugat 30 zerouri)
	
	public Polinom(int grad, int []coeficienti){ //constructor polinom (cu parametri)
		this.grad=grad;
		for(int i=0; i<=grad; i++){
			this.coeficienti[i]=coeficienti[i];
		}
	}
	public Polinom(){}                          //constructor polinom (fara parametri-mai utilizat)
	
	public void afisPolinom(){                 //metoda afisare polinom pe ecran
		for(int i=0; i<grad; i++){
			if(coeficienti[i]!=0)
			   System.out.print("("+coeficienti[i]+"*x^"+i+")+ ");
		}
		System.out.print("("+coeficienti[grad]+"*x^"+grad+")");
	}
	
	public String getString(Polinom p){       //returnare String pentru afisarea polinomului
		                                      //in casuta de rezultat
		if(p.coeficienti[p.grad]==0){
			return "Polinomul null";
		}
		String a="";
		for(int i=0; i<p.grad; i++){
			if(p.coeficienti[i]!=0)
				a=a+"("+p.coeficienti[i]+"*x^"+i+")+";
		}
		if(p.coeficienti[grad]!=0)
			a=a+"("+p.coeficienti[grad]+"*x^"+grad+")";
		return a;
	}
	
	public void construire(int coef, int putere, int gradMax){ //metoda construire (pe parcurs)
		if(gradMax>grad){                                                       //a polinomului
		  grad=gradMax;
		}
		coeficienti[putere]=coef;
	}
	
	public void stergerePolinom(Polinom p){              //metoda stergere polinom(coeficienti 0)
		for(int i=0; i<=p.grad; i++){
			p.coeficienti[i]=0;
		}
		p.grad=0;
	}
	
	public Polinom adunare(Polinom p1,Polinom p2){     // metoda adunare polinoame
		Polinom rez=new Polinom();
		if(p1.grad>p2.grad){
			rez.grad=p1.grad;
		}
		else
			rez.grad=p2.grad;
		for(int i=0; i<=rez.grad; i++){
			rez.coeficienti[i]=p1.coeficienti[i]+p2.coeficienti[i];
		}
		
		
		return rez;
	}
	public Polinom scadere(Polinom p1, Polinom p2){    //metoda scadere polinoame
		Polinom rez=new Polinom();
		if(p1.grad>p2.grad){
			rez.grad=p1.grad;
		}
		else
			rez.grad=p2.grad;
		for(int i=rez.grad; i>=0; i--){
			rez.coeficienti[i]=p1.coeficienti[i]-p2.coeficienti[i];
		}
		return rez;
	}
	
	public Polinom inmultire(Polinom p1, Polinom p2){  //metoda inmultire polinoame
		Polinom rez=new Polinom();
		rez.grad=p1.grad+p2.grad;
		for(int i=0; i<=p1.grad; i++){
			for(int j=0; j<=p2.grad; j++){
				rez.coeficienti[i+j]+=p1.coeficienti[i]*p2.coeficienti[j];
			}
		}
		
	    return rez;
	}
	
	public Polinom impartire(Polinom p1, Polinom p2){ //metoda impartire polinoame
		Polinom rez=new Polinom();         //polinoame ajutatoare
		Polinom pp=new Polinom();
		Polinom rez_partial=new Polinom();
		rez.grad=p1.grad-p2.grad;
		int i=0,nr=0;
		int grd=rez.grad;
		int oprire=grd;
		
		if(p1.grad<p2.grad){                //daca nu se poate efectua impartirea
			                                //se returneaza polinomul nul
			Polinom rezultat=new Polinom();
			rezultat.grad=0;
			rezultat.coeficienti[0]=0;
			return rezultat;
		}
		
		while(p1.grad>=p2.grad){
			if(p1.coeficienti[p1.grad]==0){
				return rez;
			}
			if(p1.coeficienti[p1.grad]!=0 && p2.coeficienti[p2.grad]!=0){
				rez.construire(p1.coeficienti[p1.grad]/p2.coeficienti[p2.grad], p1.grad-p2.grad, grd);
				
			}
			
			//int j=rez.coeficienti[rez.grad]+p2.coeficienti[p2.grad];
			if(oprire==0 ||(p1.grad==(rez.grad+p2.grad) && p1.coeficienti[p1.grad]==0)) {
			   return rez;
			}
			oprire--;
			
			rez_partial=rez;
			rez_partial.grad=rez.grad-i;
			pp=pp.inmultire(rez, p2);
		    
			p1=p1.scadere(p1, pp);

			p1.grad--;
			i++;
		}
	
		return rez;
	}
	
	public Polinom derivare(Polinom p1){            //metoda derivare polinom
		Polinom rez=new Polinom();
		if(p1.grad==0) { rez.grad=0; rez.coeficienti[0]=0;}
		  else if(p1.grad==1) {rez.coeficienti[0]=p1.coeficienti[1]; rez.grad=0;}
		  else{
		   rez.grad=p1.grad-1;
		   for(int i=p1.grad; i>=1; i--){
			   rez.coeficienti[i-1]=p1.coeficienti[i]*i;
		   }
		  }
		return rez;
	}
	public Polinom integrare(Polinom p1){          //metoda integrare polinom
		Polinom rez=new Polinom();
		rez.grad=p1.grad+1;
		for(int i=p1.grad; i>=0; i--){
			rez.coeficienti[i+1]=p1.coeficienti[i]/(i+1);
		}
		while(rez.coeficienti[rez.grad]==0){     //in cazul cand avem coeficienti egali cu 0
			                                     //la gradul maxim, scadem gradul polinomului
			rez.grad--;
		}
		return rez;
	}
	
	public int compararePolinoame(Polinom p1,Polinom p2){
		if(p1.grad!=p2.grad) return 0;
		for(int i=0; i<=p1.grad; i++){
			if(p1.coeficienti[i]!=p2.coeficienti[i])
				return 0;
		}
		return 1;
		
	}
	
	public int valoarePolinom(int punct, Polinom p){
		int rez=0;
		for(int i=0; i<=p.grad; i++){
			int rezPartial=1;
			for(int j=0; j<i; j++)
				rezPartial*=punct;
			rezPartial*=p.coeficienti[i];
			rez+=rezPartial;
		}
		return rez;
	}
	
	
}
